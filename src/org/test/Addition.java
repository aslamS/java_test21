package org.test;

public class Addition {
	public static void main(String[] args) {
		int num = 1215;
		int sum = 0;
		int reverse = 0;

		while (num > 0) { //1215>0 t
			int rem = num % 10;  //1215%10=5 
			sum = sum + rem;  //rem=5 
			reverse = reverse * 10 + rem; 

			num = num / 10; 

		}
		System.out.println("Reverse order is :	" + reverse);
		System.out.println("Total value is :" + sum);
	}

}
