package org.test;

import java.util.Arrays;

public class Question1 {
	public static void main(String[] args) {
		int[] arr = { 2, 9, 5, 0 };
		int n = 4;
		for (int i = 0; i < arr.length; i++) { //i=0=2, length=4 index=3 
			if (i == arr.length - 1) {//0==4-1=3   0==3,1==3,2==3,3==3 
				arr[i] = n;           //0=4
			}
		}
		System.out.println(Arrays.toString(arr));
	}

}
