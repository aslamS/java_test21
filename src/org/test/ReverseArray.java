package org.test;

public class ReverseArray {
	public static void main(String[] args) {
		int[] arr = { 11, 19, 15, 10 };
		{
			for (int i = arr.length - 1; i >= 0; i--) { // length =4 i=4-1=3,2 , 3,2>=0

				System.out.print(arr[i] + " "); // arr[i]=[10,15]
			}
		}
	}
}